class W_App{
  
  var name;
  dynamic category;
  var developer;
  int year = 2012;

  W_App(this.name, this.category, this.developer, this.year);

  void tranformName(){
    print(this.name.toUpperCase());
  }

}

void main(){

  List<W_App> appList = [W_App('Amabani', 'Best Gaming Solution','Mukundi Lambani',2021), W_App('EasyEquities', 'Best Consumer Solution','Charles Savage',2020), W_App('Naked Insurance', 'Best Financial Solution','Sumarie Greybe and Ernest North and Alex Thomson',2019), W_App('Khula', 'Best Agriculture Solution','Karidas Tshintsholo and Matthew Pipe',2018), W_App('Standard Bank Shyft', 'Best Financial Solution','Arno von Helden',2017), W_App('Domestly', 'Best Consumer App','Berno Potgieter and Thatoyaona Marumo',2016), W_App('WumDrop', 'Best Enterprise App','Simon Hartley and Roy Borole',2015), W_App('Live Inspect', 'Best Android Enterprise App','Lightstone',2014), W_App('SnapScan', 'HTML5','Kobus Ehlers',2013), W_App('FNB Banking', 'Best Consumer iOS App','First National Bank',2012),];

  print(appList[1].name);
  print(appList[1].category);
  print(appList[1].developer);
  print(appList[1].year);

  appList[1].tranformName();
}